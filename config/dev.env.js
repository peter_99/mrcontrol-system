'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    BASE_API: '"http://localhost:9528/svr"' // 跨域
    // BASE_API: '"http://192.168.0.110:9528/svr"' // 跨域
})
