import {login, getUserInfo, getUserRoles} from '@/api/global/index'
import {getLocalStorage} from '@/utils/filters'
import Cookie from 'js-cookie'
import axios from 'axios'

const state = {
    // token
    token: Cookie.get('token'),
    // 用户姓名
    realName: getLocalStorage('userInfo', 'realName'),
    // 权限列表
    roles: [],
    // 重新渲染菜单
    reRender: 0,
    // 是否显示全局筛选框
    showSearch: true,
    // 是否是数据管理病案包，筛选框不同
    isDataPackage: false,
    // 病案详情是否是编辑状态
    isEdit: false
}

const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles
    },
    SET_RERENDER: (state) => {
        state.reRender++
    },
    SET_SHOWSEARCH: (state, val) => {
        state.showSearch = val
    },
    SET_ISDATAPACKAGE: (state, val) => {
        state.isDataPackage = val
    },
    SET_ISEDIT: (state, val) => {
        state.isEdit = val
    }
}
const actions = {
    // 用户登录（ 该方法名字在映射的时候会被改写 user/login => login ）
    login ({commit}, userInfo) {
        const {username, password} = userInfo
        return new Promise((resolve, reject) => {
            let params = {
                username,
                password
            }
            login(params).then(res => {
                if (res.code.toString() === '200') {
                    let userInfo = {
                        token: res.data.token,
                        realName: res.data.用户姓名,
                        mechanism: res.data.组织机构,
                        checkAuth: res.data.查阅权限
                    }
                    Cookie.set('token', res.data.token, {expires: 1})
                    axios.defaults.headers['token'] = res.data.token
                    localStorage.setItem('userInfo', JSON.stringify(userInfo))
                    localStorage.setItem('rolesCode', JSON.stringify(res.data.权限))
                    localStorage.setItem('userMechanism', JSON.stringify(res.data.所属机构))
                    localStorage.setItem('userType', JSON.stringify(res.data.角色.角色类型))
                    resolve(res)
                } else {
                    resolve(res)
                }
            })
                .catch(err => {
                    reject(err)
                })
        })
    },

    // 重新获取用户信息
    getUserInfoAgain ({dispatch}) {
        return new Promise((resolve, reject) => {
            Promise.all([dispatch('getLoginUserInfo'), dispatch('getLoginUserRoles')]).then(res => {
                resolve(res)
            })
        })
    },

    // 获取用户信息
    getLoginUserInfo ({commit}) {
        return new Promise((resolve, reject) => {
            getUserInfo().then(res => {
                if (res.code.toString() === '200') {
                    let userInfo = {
                        realName: res.data.userName,
                        mechanism: res.data.userPowerOrgan,
                        checkAuth: res.data.userAccess
                    }
                    localStorage.setItem('userInfo', JSON.stringify(userInfo))
                    localStorage.setItem('userMechanism', JSON.stringify(res.data.userOrgan))
                    localStorage.setItem('userType', JSON.stringify(res.data.userRole.roleType))
                    let orgId = getLocalStorage('globalSearchQC', 'orgId')
                    // 查询详情时间限制
                    let index = res.data.userPowerOrgan.findIndex(item => item.id === orgId)
                    let viewDeadlineDays = res.data.userPowerOrgan[index].viewDeadlineDays
                    localStorage.setItem('viewDeadlineDays', viewDeadlineDays)
                    // 科室重置或选中
                    let flagOrg = res.data.userPowerOrgan.filter(item => item.id === orgId)
                    let depId = getLocalStorage('globalSearchQC', 'departmentId')
                    let flagDep = JSON.stringify(res.data.userPowerOrgan).indexOf(depId) === -1
                    if (!flagOrg.length) {
                        localStorage.setItem('globalSearchQC', JSON.stringify({
                            ...getLocalStorage('globalSearchQC'),
                            orgId: '',
                            departmentId: '',
                            department: '',
                            departmentName: '',
                            coder: ''
                        }))
                    } else if (flagDep) {
                        localStorage.setItem('globalSearchQC', JSON.stringify({
                            ...getLocalStorage('globalSearchQC'),
                            departmentId: '',
                            department: '',
                            departmentName: ''
                        }))
                    }
                    resolve(res)
                } else {
                    resolve(res)
                }
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 获取用户拥有权限
    getLoginUserRoles ({commit}) {
        return new Promise((resolve, reject) => {
            getUserRoles().then(res => {
                if (res.code.toString() === '200') {
                    let arr = []
                    res.data.forEach(item => {
                        arr.push(parseInt(item.id))
                    })
                    arr.push(999999999999)
                    localStorage.setItem('rolesCode', JSON.stringify(arr))
                    sessionStorage.setItem('lastRequest', new Date().getTime())
                    resolve(res)
                } else {
                    resolve(res)
                }
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 退出登录
    logout ({commit}) {
        return new Promise((resolve, reject) => {
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            localStorage.removeItem('globalSearchDRG')
            localStorage.removeItem('globalFormDRG')
            localStorage.removeItem('globalSearchQC')
            localStorage.removeItem('globalFormQC')
            localStorage.removeItem('userInfo')
            localStorage.removeItem('userMechanism')
            localStorage.removeItem('userType')
            localStorage.removeItem('viewDeadlineDays')
            localStorage.removeItem('globalSearch')
            localStorage.removeItem('globalSearchQC')
            localStorage.removeItem('rolesCode')
            localStorage.removeItem('tagsView')
            localStorage.removeItem('tagsViewQC')
            localStorage.removeItem('tagsViewMS')
            localStorage.removeItem('searchForm')
            localStorage.removeItem('searchFormQC')
            localStorage.removeItem('searchFormMS')
            localStorage.removeItem('listQuery')
            localStorage.removeItem('cloudMode')
            Cookie.remove('sidebarStatus_drg')
            Cookie.remove('sidebarStatus')
            Cookie.remove('size')
            Cookie.remove('timeLeft')
            Cookie.remove('initlogout')
            Cookie.remove('token')
            Cookie.remove('isPackage')
            Cookie.set('optOut', 'optOut')
            sessionStorage.clear()
            resolve()
        })
    },
    // 重新渲染组件
    updateReRender ({commit}) {
        commit('SET_RERENDER')
    },
    // 是否显示全局筛选框
    isShowSearch ({commit}, val) {
        commit('SET_SHOWSEARCH', val)
    },
    // 数据管理病案包筛选框
    showPackage ({commit}, val) {
        Cookie.set('isPackage', val)
        commit('SET_ISDATAPACKAGE', val)
    },
    // 修改编辑状态
    changeEditState ({commit}, val) {
        commit('SET_ISEDIT', val)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
