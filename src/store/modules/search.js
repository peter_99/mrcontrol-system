const state = {
    searchForm: {
        QualityOverview: {},
        OverviewIndex: {},
        OverviewList: {},
        OverviewListIndex: {},
        OverviewListInfo: {},
        ImportMR: {},
        ImportMRIndex: {},
        ImportMRList: {},
        ImportMRListIndex: {},
        ImportMRListInfo: {},
        MRList: {},
        MRListIndex: {},
        MRListInfo: {},
        QualityControl: {},
        Score: {},
        ScoreIndex: {},
        ScoreList: {},
        ScoreListIndex: {},
        ScoreListInfo: {},
        CodingDefect: {},
        CodingIndex: {},
        CodingList: {},
        CodingListIndex: {},
        CodingListInfo: {},
        NonCodingDefect: {},
        NonCodingIndex: {},
        NonCodingList: {},
        NonCodingListIndex: {},
        NonCodingListInfo: {},
        SummaryList: {},
        DepSummary: {},
        DepSummaryList: {},
        DepSummaryMR: {},
        DepSummaryMRList: {},
        DepSummaryMRInfo: {},
        DepSummaryAnalysis: {},
        DepSummaryAnalysisIndex: {},
        DepSummaryAnalysisMR: {},
        DepSummaryAnalysisMRList: {},
        DepSummaryAnalysisMRInfo: {},
        DoctorSummary: {},
        DoctorSummaryList: {},
        DoctorSummaryMR: {},
        DoctorSummaryMRList: {},
        DoctorSummaryInfo: {},
        DoctorSummaryAnalysis: {},
        DoctorSummaryAnalysisIndex: {},
        DoctorSummaryAnalysisMR: {},
        DoctorSummaryAnalysisMRList: {},
        DoctorSummaryAnalysisMRInfo: {},
        CoderSummary: {},
        CoderSummaryList: {},
        CoderSummaryMR: {},
        CoderSummaryMRList: {},
        CoderSummaryMRInfo: {},
        CoderSummaryAnalysis: {},
        CoderSummaryAnalysisIndex: {},
        CoderSummaryAnalysisMR: {},
        CoderSummaryAnalysisMRList: {},
        CoderSummaryAnalysisMRInfo: {},
        QCEffect: {},
        QCEffectIndex: {},
        QCEffectInfo: {},
        QCEffectMR: {},
        QCEffectMRList: {},
        QCEffectMRInfo: {},
        QCEffectCode: {},
        QCEffectNonCode: {},
        MyMRAuditIndex: {},
        MyMRAuditInfo: {},
        MyMRCollectIndex: {},
        MyMRCollectInfo: {}
    }
}

const mutations = {
    SET_SEARCH_FORM_ITEM: (state, form) => {
        state.searchForm[form.pageName] = form.query
    },
    COVER_SEARCH_FORM_ITEM: (state, form) => {
        state.searchForm[form.pageName] = form.query
    },
    SET_SEARCH_FORM: (state, form) => {
        state.searchForm = form
    },
    CLEAR_ALL: (state, name) => {
        Object.keys(state.searchForm).forEach(key => {
            if (key !== name) {
                state.searchForm[key] = {}
            }
        })
    }
}

const actions = {
    setSearchFormItem ({ commit }, form) {
        commit('SET_SEARCH_FORM_ITEM', form)
    },
    coverSearchFormItem ({ commit }, form) {
        commit('COVER_SEARCH_FORM_ITEM', form)
    },
    setSearchForm ({ commit }, form) {
        commit('SET_SEARCH_FORM', form)
    },
    clearAll ({ commit }, name) {
        commit('CLEAR_ALL', name)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
