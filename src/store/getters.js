const getters = {
    loading: state => state.app.loading,
    loadingText: state => state.app.loadingText,
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    token: state => state.user.token,
    roles: state => state.user.roles,
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews
}
export default getters
