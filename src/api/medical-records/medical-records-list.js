import request from '@/utils/request'

/**
 * 获取总览列表(总览)
 */
export function getOverviewList (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/MedicalRecordList',
        method: 'post',
        data
    })
}

/**
 *  列表导出
 */
export function exportList (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/ExportToList',
        method: 'post',
        data
    })
}

/**
 *  导出卫统4
 */
export function exportWT4 (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/ExportToWT4',
        method: 'post',
        data
    })
}

/**
 *  导出HQMS
 */
export function exportHQMS (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/ExportToHQMS',
        method: 'post',
        data
    })
}

/**
 *  导出三级医院标准
 */
export function exportSJJX (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/ExportToSJJX',
        method: 'post',
        data
    })
}
