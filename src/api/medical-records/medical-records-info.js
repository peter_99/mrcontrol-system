import request from '@/utils/request'

/**
 * 获取质控效果分析
 */
export function getQCMenuData (data) {
    return request({
        url: '/MRQC/api/QCAnalysis/QCResult',
        method: 'post',
        data
    })
}

/**
 * 获取DRG信息
 */
export function getDRGData (data) {
    return request({
        url: '/DRG/api/DataOverview/DRGDetailByID',
        method: 'post',
        data
    })
}

/**
 * 获取病案信息（表格）
 */
export function MRTableData (data) {
    return request({
        url: '/MRDM/Api/MRPackage/GetDetail',
        method: 'post',
        data
    })
}

/**
 * 获取病案质控的详情
 */
export function MRQCData (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/QCResult',
        method: 'post',
        data
    })
}

/**
 * 获取我的病案审核列表的详情
 */
export function MRQCAuditData (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/AuditDetail',
        method: 'post',
        data
    })
}

/**
 * 获取我的病案收藏列表的详情
 */
export function MRQCCollectData (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/CollectionDetail',
        method: 'post',
        data
    })
}

/**
 * 获取病案审核记录和收藏
 */
export function getMRState (data) {
    return request({
        url: `/MRQC/Api/MR/QueryExamine?medicalRecordID=${data.medicalRecordID}`,
        method: 'get',
        data
    })
}
