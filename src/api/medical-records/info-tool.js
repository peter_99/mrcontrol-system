import request from '@/utils/request'

/**
 * 忽略
 */
export function ignoreData (data) {
    return request({
        url: `/MRQC/Api/MR/IgnoreVerificationItem?id=${data.id}`,
        method: 'get',
        data
    })
}

/**
 * 取消忽略
 */
export function cancelIgnoreData (data) {
    return request({
        url: `/MRQC/Api/MR/CancelIgnoreVerificationItem?id=${data.id}`,
        method: 'get',
        data
    })
}

/**
 * 提交意见反馈
 */
export function saveFeedback (data) {
    return request({
        url: '/MRMS/Api/Feedback/Save',
        method: 'post',
        data
    })
}

/**
 * 打开得分列表获取数据
 */
export function getScoreList (data) {
    return request({
        url: '/MRQC/Api/MR/QueryScoreItems',
        method: 'post',
        data
    })
}

/**
 * 修改分数后获取分数
 */
export function getScoreNum (data) {
    return request({
        url: '/MRQC/Api/MR/GetScore',
        method: 'post',
        data
    })
}

/**
 * 提交修改分数
 */
export function submitScoreNum (data) {
    return request({
        url: '/MRQC/Api/MR/MakeScore',
        method: 'post',
        data
    })
}

/**
 * ICD10查询
 */
export function searchICD10 ({keyWord, pageCount, pageIndex}) {
    return request({
        url: `/ICD/api/ICD10/Disease?keyWord=${keyWord}&pageCount=${pageCount}&pageIndex=${pageIndex}`,
        method: 'get'
    })
}

/**
 * ICD10查询医保
 */
export function searchICD10YB (data) {
    return request({
        url: '/ICD/api/ICD10/DiseaseYB',
        method: 'post',
        data
    })
}

/**
 * ICD9查询
 */
export function searchICD9 ({keyWord, pageCount, pageIndex, type, selectivity}) {
    return request({
        url: `/ICD/api/ICD9/Disease?keyWord=${keyWord}&pageCount=${pageCount}&pageIndex=${pageIndex}&type=${type}&selectivity=${selectivity}`,
        method: 'get'
    })
}

/**
 * ICD9查询医保
 */
export function searchICD9YB (data) {
    return request({
        url: '/ICD/api/ICD9/DiseaseYB',
        method: 'post',
        data
    })
}

/**
 * 临床反馈
 */
export function submitFeedbackMR (data) {
    return request({
        url: '/MRQC/Api/Message/Post',
        method: 'post',
        data
    })
}

/**
 * 重新分组
 */
export function reDRGSingle (data) {
    return request({
        url: '/DRG/api/DataManage_Package/ReGroupingDetail',
        method: 'post',
        data
    })
}
