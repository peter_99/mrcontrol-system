import request from '@/utils/request'

/**
 * 病案历史查询
 */
export function searchHistory (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/SingleQCResult',
        method: 'post',
        data
    })
}

/**
 * 质控效果分析审核
 */
export function checkOverviewTopQC (data) {
    return request({
        url: '/MRQC/api/QCAnalysis/Noted',
        method: 'post',
        data
    })
}

/**
 * 审核列表详情审核
 */
export function checkAuditTopQC (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/AuditDetailNoted',
        method: 'post',
        data
    })
}

/**
 * 收藏列表详情审核
 */
export function checkCollectTopQC (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/AuditCollectionNoted',
        method: 'post',
        data
    })
}

/**
 * 审核
 */
export function checkOverviewTop (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/Noted',
        method: 'post',
        data
    })
}
