import request from '@/utils/request'

/**
 * 质控预览
 */
export function previewData (data) {
    return request({
        url: '/MRDM/Api/MRReport/DataPreview',
        method: 'post',
        data
    })
}

/**
 * 质控预览提交
 */
export function sumbitPreviewData (data) {
    return request({
        url: '/MRDM/Api/MRReport/SavePreview',
        method: 'post',
        data
    })
}
