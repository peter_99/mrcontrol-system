import request from '@/utils/request'

/**
 *  获取汇总列表科室汇总
 */
export function getSummaryDepList (data) {
    return request({
        url: '/MRQC/api/Summary/DepSummaryList',
        method: 'post',
        data
    })
}

/**
 *  导出汇总列表科室汇总
 */
export function exportSummaryDepList (data) {
    return request({
        url: '/MRQC/api/Summary/DepExcel',
        method: 'post',
        data,
        responseType: 'blob'
    })
}

/**
 *  汇总分析列表数据
 */
export function getSummaryAnalysisList (data) {
    return request({
        url: '/MRQC/api/Summary/SummaryAnalysisList',
        method: 'post',
        data
    })
}

/**
 *  导出汇总分析列表数据
 */
export function exportSummaryAnalysisList (data) {
    return request({
        url: '/MRQC/api/Summary/SummaryExcel',
        method: 'post',
        data,
        responseType: 'blob'
    })
}
