import request from '@/utils/request'

/**
 *  获取汇总列表医师汇总
 */
export function getSummaryDoctorList (data) {
    return request({
        url: '/MRQC/api/Summary/ResidentSummaryList',
        method: 'post',
        data
    })
}

/**
 *  导出汇总列表医师汇总
 */
export function exportSummaryDoctorList (data) {
    return request({
        url: '/MRQC/api/Summary/ResidentExcel',
        method: 'post',
        data,
        responseType: 'blob'
    })
}
