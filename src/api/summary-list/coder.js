import request from '@/utils/request'

/**
 *  获取汇总列表编码员汇总
 */
export function getSummaryCoderList (data) {
    return request({
        url: '/MRQC/api/Summary/CoderSummaryList',
        method: 'post',
        data
    })
}

/**
 *  导出汇总列表编码员汇总
 */
export function exportSummaryCoderList (data) {
    return request({
        url: '/MRQC/api/Summary/CoderExcel',
        method: 'post',
        data,
        responseType: 'blob'
    })
}
