import request from '@/utils/request'
import Cookie from 'js-cookie'

/**
 * 登录
 */
export function login (data) {
    return request({
        url: '/MRQCold/api/account/login',
        method: 'post',
        data
    })
}

/**
 * 退出登录
 */
export function logout (data) {
    return request({
        url: '/CAS/Api/V1/Account/Logout',
        method: 'get',
        data
    })
}

/**
 * 获取编码员列表
 */
export function getCoderList (data) {
    return request({
        url: `/MRQC/api/Overview/Coder?orgId=${data.orgId}`,
        method: 'get',
        data
    })
}

/**
 * 获取住院医师列表
 */
export function getDoctorList (data) {
    return request({
        url: `/MRQC/api/Overview/Resident?orgId=${data.orgId}&department=${data.department}`,
        method: 'get',
        data
    })
}

/**
 * 获取用户列表
 */
export function getLastUserList (data) {
    return request({
        url: `/MRMS/Api/MrUser/GetOrgUser?OrgId=${data.orgId}`,
        method: 'get',
        data
    })
}

/**
 * 获取质控总数
 */
export function getTotal (data) {
    return request({
        url: `/MRQC/api/Overview/TotalRecord?orgId=${data.orgId}`,
        method: 'get',
        data
    })
}

/**
 * 修改密码
 */
export function changePassword (data) {
    return request({
        url: '/MRMS/Api/MrUser/EditPassword',
        method: 'post',
        data
    })
}

/**
 * 获取系统版本授权信息
 */
export function getSystemInfo (data) {
    return request({
        url: '/CAS/Api/V1/License/GetInfo',
        method: 'get',
        data
    })
}

/**
 * 搜索病案号
 */
export function getMRNumberInfo (data) {
    return request({
        url: '/MRQCold/api/MR/QuickSearch',
        method: 'post',
        data
    })
}

/**
 * 获取用户信息，机构信息等
 */
export function getUserInfo (data) {
    return request({
        url: '/MRMS/api/MrUser/GetLoginUser',
        method: 'get',
        data,
        headers: {'Authorization': 'Bearer ' + Cookie.get('token')}
    })
}

/**
 * 获取用户拥有权限
 */
export function getUserRoles (data) {
    return request({
        url: '/MRMS/api/MrUser/GetUserPower',
        method: 'get',
        data,
        headers: {'Authorization': 'Bearer ' + Cookie.get('token')}
    })
}

/**
 * 获取头部搜索病案
 */
export function MRQCData (data) {
    return request({
        url: '/MRQC/api/Overview/QCDetail',
        method: 'post',
        data
    })
}
