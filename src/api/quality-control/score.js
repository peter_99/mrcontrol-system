import request from '@/utils/request'

/**
 *  获取评分总览
 */
export function getScoreAll (data) {
    return request({
        url: '/MRQC/api/ScoreStatistics/Overview',
        method: 'post',
        data
    })
}

/**
 *  获取评分趋势分析
 */
export function getScoreTrend (data) {
    return request({
        url: '/MRQC/api/ScoreStatistics/Trend',
        method: 'post',
        data
    })
}

/**
 *  获取评分top10
 */
export function getScoreTop10 (data) {
    return request({
        url: '/MRQC/api/ScoreStatistics/Top10',
        method: 'post',
        data
    })
}
