import request from '@/utils/request'

/**
 *  获取非编码缺陷总览
 */
export function getNonCodingDefectAll (data) {
    return request({
        url: '/MRQC/api/NoneCodingStatistics/Overview',
        method: 'post',
        data
    })
}

/**
 *  获取非编码缺陷走势分析
 */
export function getNonCodingDefectTrend (data) {
    return request({
        url: '/MRQC/api/NoneCodingStatistics/Trend',
        method: 'post',
        data
    })
}

/**
 *  获取非编码缺陷top10
 */
export function getNonCodingDefectTop10 (data) {
    return request({
        url: '/MRQC/api/NoneCodingStatistics/Top10',
        method: 'post',
        data
    })
}
