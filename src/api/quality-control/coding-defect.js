import request from '@/utils/request'

/**
 *  获取编码缺陷总览
 */
export function getCodingDefectAll (data) {
    return request({
        url: '/MRQC/api/CodingStatistics/Overview',
        method: 'post',
        data
    })
}

/**
 *  获取编码缺陷走势分析
 */
export function getCodingDefectTrend (data) {
    return request({
        url: '/MRQC/api/CodingStatistics/Trend',
        method: 'post',
        data
    })
}

/**
 *  获取编码缺陷top10
 */
export function getCodingDefectTop10 (data) {
    return request({
        url: '/MRQC/api/CodingStatistics/Top10',
        method: 'post',
        data
    })
}
