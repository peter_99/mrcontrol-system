import request from '@/utils/request'

/**
 *  获取质控结果分析概览
 */
export function getQCOverview (data) {
    return request({
        url: '/MRQC/api/QCAnalysis/Overview',
        method: 'post',
        data
    })
}

/**
 *  获取质控结果分析对比
 */
export function getQCCompare (data) {
    return request({
        url: '/MRQC/api/QCAnalysis/Comparison',
        method: 'post',
        data
    })
}

/**
 *  获取质控结果分析表格
 */
export function getModifiedList (data) {
    return request({
        url: '/MRQC/api/QCAnalysis/ModifiedList',
        method: 'post',
        data
    })
}

/**
 *  获取质控结果分析表格编码修改项
 */
export function getModifiedCodeList (data) {
    return request({
        url: `/MRQC/api/QCAnalysis/CodingModifiedDetail?medicalRecordID=${data.medicalRecordID}`,
        method: 'get',
        data
    })
}

/**
 *  获取质控结果分析表格非编码修改项
 */
export function getModifiedNonCodeList (data) {
    return request({
        url: `/MRQC/api/QCAnalysis/NoneCodingModifiedDetail?medicalRecordID=${data.medicalRecordID}`,
        method: 'get',
        data
    })
}

/**
 * 导出修改病案列表
 */
export function exportEditTable (data) {
    return request({
        url: '/MRQC/api/QCAnalysis/ExportModified',
        method: 'post',
        data,
        responseType: 'blob'
    })
}
