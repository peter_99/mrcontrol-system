import request from '@/utils/request'

/**
 *  获取我的收藏列表
 */
export function getMyMRCollectList (data) {
    return request({
        url: '/MRQC/api/MyMedicalRecord/CollectionList',
        method: 'post',
        data
    })
}

/**
 *  收藏/取消收藏病案
 */
export function collectMR (data) {
    return request({
        url: '/MRQC/api/MyMedicalRecord/EditCollection',
        method: 'post',
        data
    })
}

/**
 *  列表导出
 */
export function exportList (data) {
    return request({
        url: '/MRQC/api/MedicalRecord/ExportToList',
        method: 'post',
        data,
        responseType: 'blob'
    })
}
