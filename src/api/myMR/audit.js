import request from '@/utils/request'

/**
 *  获取我的审核列表
 */
export function getMyMRAuditList (data) {
    return request({
        url: '/MRQC/api/MyMedicalRecord/MedicalRecordList',
        method: 'post',
        data
    })
}

/**
 *  列表导出
 */
export function exportList (data) {
    return request({
        url: '/MRQC/api/MyMedicalRecord/ExportAuditList',
        method: 'post',
        data
    })
}
