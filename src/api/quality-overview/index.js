import request from '@/utils/request'

/**
 *  获取首页总览
 */
export function getQualityAll (data) {
    return request({
        url: '/MRQC/api/Overview/Overview',
        method: 'post',
        data
    })
}

/**
 *  获取首页top10
 */
export function getQualityTop10 (data) {
    return request({
        url: '/MRQC/api/Overview/Top10',
        method: 'post',
        data
    })
}

/**
 *  获取首页质控分析
 */
export function getQualityTrend (data) {
    return request({
        url: '/MRQC/api/Overview/Trend',
        method: 'post',
        data
    })
}
