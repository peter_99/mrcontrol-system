import request from '@/utils/request'

/**
 * 获取病案包数据
 */
export function getDataPackage (data) {
    return request({
        url: '/MRQC/api/Package/Package',
        method: 'post',
        data
    })
}

/**
 * 获取数据列表
 */
export function getPageDataList (data) {
    return request({
        url: '/MRQC/api/Overview/DetailList',
        method: 'post',
        data
    })
}

/**
 * 获取多文件上传
 */
export function uploadMultipleFiles (data) {
    return request({
        url: '/MRDM/Api/MRPackage/BachUpload',
        method: 'post',
        data,
        headers: {'Content-Type': 'multipart/form-data'}
    })
}

/**
 * 获取单文件上传，指定id
 */
export function uploadSingleFile (data, id) {
    return request({
        url: `/MRDM/Api/MRPackage/SingleUpload?id=${id}`,
        method: 'post',
        data,
        headers: {'Content-Type': 'multipart/form-data'}
    })
}

/**
 * 导出数据列表
 */
export function downloadDataList (data) {
    return request({
        url: '/MRQC/api/Overview/ExportDetailList',
        method: 'post',
        data,
        responseType: 'blob'
    })
}

/**
 * 导出病案包数据
 */
export function downloadPackageList (data) {
    return request({
        url: '/MRQC/api/Package/ExportPackage',
        method: 'post',
        data
    })
}

/**
 * 删除病案包数据
 */
export function removePackage (data) {
    return request({
        url: `/MRDM/Api/MRPackage/DelPackage?ID=${data.id}`,
        method: 'get',
        data
    })
}

/**
 * 重新质控病案包
 */
export function reQC (data) {
    return request({
        url: `/MRQC/api/Package/ReQualityPackage?MRPackageId=${data}`,
        method: 'get',
        data
    })
}

/**
 * 重新质控单条病案数据
 */
export function reQCData (data) {
    return request({
        url: `/MRQC/Api/Package/ReQualityDetail?MedicalRecordID=${data}`,
        method: 'get',
        data
    })
}
