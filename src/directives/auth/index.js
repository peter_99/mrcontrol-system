// 按钮权限指令
import Vue from 'vue'

function auth (val) {
    let btnPowerArr = JSON.parse(localStorage.getItem('rolesCode'))
    if (btnPowerArr === undefined || btnPowerArr === null) {
        return false
    }
    return btnPowerArr.indexOf(val) !== -1
    // return true
}

export default {
    bind: function (el, binding) {
        let path = binding.value.code
        if (!auth(path)) {
            Vue.nextTick(() => {
                el.parentNode.removeChild(el)
            })
        }
    }
}
