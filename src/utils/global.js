import { parseTime, removeSpaces, getLocalStorage, setGetStore, setSession, getSession } from '@/utils/filters'

// 全局方法
const methods = {parseTime, removeSpaces, getLocalStorage, setGetStore, setSession, getSession}
// 全局过滤器
const filters = {parseTime}

export default {
    install (Vue) {
        Object.keys(methods).forEach(key => { Vue.prototype[key] = methods[key] })
        Object.keys(filters).forEach(key => { Vue.filter(key, filters[key]) })
    }
}
