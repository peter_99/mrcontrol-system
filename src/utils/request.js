import axios from 'axios'
import store from '../store'
import Cookie from 'js-cookie'
import { Message } from 'element-ui'
import Vue from 'vue'

let baseURL = process.env.BASE_API
if (process.env.NODE_ENV !== 'production') {
    baseURL = localStorage.getItem('baseURL') ? localStorage.getItem('baseURL') : baseURL
}
// 创建axios实例
let service = axios.create({
    // api 的 base_url
    baseURL: baseURL,
    // 请求超时时间
    timeout: 300000
})

if (Cookie.get('token')) {
    // 让每个请求携带 token 请根据实际情况自行修改
    service.defaults.headers['Authorization'] = 'Bearer ' + Cookie.get('token')
}

let count = 0
// request拦截器
service.interceptors.request.use(
    config => {
        let url = window.location.href
        if (!url.includes('/login') && !config.url.includes('/Overview/Coder') && !config.url.includes('/Overview/Resident') && !config.url.includes('/Overview/TotalRecord')) {
            // 开启Loading
            // 不是执行导出
            if (!store.getters.isExport) {
                count++
                store.dispatch('UpdateLoading', true)
            }
        }
        config.data = Vue.prototype.removeSpaces(config.data)
        if (config.data) {
            delete config.data.dep
            delete config.data.timeUnit
            delete config.data.yearMonth
            delete config.data.quarter
            delete config.data.tab
            if (config.data.department === '') {
                config.data.department = null
            }
            if (config.data.start && config.data.start.length === 10) {
                config.data.start = config.data.start + ' 00:00:00'
            }
            if (config.data.end && config.data.end.length === 10) {
                config.data.end = config.data.end + ' 23:59:59'
            }
            if (config.data.department && config.data.department === config.data.departmentName) {
                config.data.departmentName = ''
            }
            if (config.data.orderBy && config.data.orderBy.includes('departmentName')) {
                config.data.orderBy = config.data.orderBy.replace('departmentName', 'departmentCode')
            }
        }
        return config
    },
    error => {
        // 不是执行导出
        if (!store.getters.isExport) {
            count++
            store.dispatch('UpdateLoading', true)
        }
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// 全局拦截响应
service.interceptors.response.use(
    response => {
        let url = window.location.href
        // 不是执行导出
        if (!store.getters.isExport) {
            if (!url.includes('/login') && !response.request.responseURL.includes('/Overview/Coder') && !response.request.responseURL.includes('/Overview/Resident') && !response.request.responseURL.includes('/Overview/TotalRecord')) {
                count--
            }
            // 关闭Loading
            if (count <= 0) {
                store.dispatch('UpdateLoading', false)
            } else {
                store.dispatch('UpdateLoading', true)
            }
        }
        return new Promise(resolve => {
            if (response.data.code && response.data.code.toString() === '401') {
                Cookie.set('timeOut', 'timeOut')
                store.dispatch('user/logout').then(() => {
                    // 为了重新实例化vue-router对象 避免bug
                    location.reload()
                })
            } else {
                resolve(response.data)
            }
        })
    },
    error => {
        // 不是执行导出
        if (!store.getters.isExport) {
            count--
            if (count <= 0) {
                store.dispatch('UpdateLoading', false)
            } else {
                store.dispatch('UpdateLoading', true)
            }
        }
        if (error.message.includes('401')) {
            Cookie.set('timeOut', 'timeOut')
            store.dispatch('user/logout').then(() => {
                // 验证失败，请重新登录
                window.location.href = '/login'
            })
        }
        if (error.message.includes('403')) {
            Message.error('无权限操作')
        }
        return Promise.reject(error)
    }
)
export default service
