import store from '@/store'

/**
 * 解析时间为字符串
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
export function parseTime (time, cFormat) {
    if (time === '' || time === null || time === undefined || time === [] || time === {}) {
        return ''
    }
    if (arguments.length === 0) {
        return null
    }
    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
    let date
    if (typeof time === 'object') {
        date = time
    } else {
        if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
            time = parseInt(time)
        }
        if ((typeof time === 'number') && (time.toString().length === 10)) {
            time = time * 1000
        }
        date = new Date(time)
    }
    const formatObj = {
        y: date.getFullYear(),
        m: date.getMonth() + 1,
        d: date.getDate(),
        h: date.getHours(),
        i: date.getMinutes(),
        s: date.getSeconds(),
        a: date.getDay()
    }
    const timeStr = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
        let value = formatObj[key]
        // Note: getDay() returns 0 on Sunday
        if (key === 'a') {
            return ['日', '一', '二', '三', '四', '五', '六'][value]
        }
        if (result.length > 0 && value < 10) {
            value = '0' + value
        }
        return value || 0
    })
    return timeStr
}

/**
 * 日期缩写字符串20200101转日期格式
 * @param data 日期数字
 */
export function dateNumFormat (data) {
    if (data) {
        let d = data.replace(/[^0-9]/ig, '')
        let year = d.substr(0, 4) ? d.substr(0, 4) : ''
        let month = d.substr(4, 2) ? '-' + d.substr(4, 2) : ''
        let date = d.substr(6, 2) ? '-' + d.substr(6, 2) : ''
        return year + month + date
    } else {
        return ''
    }
}

/**
 * 去除字符串头尾空格
 * @param data 数据源
 */
export function removeSpaces (data) {
    if (data === null || data === undefined) {
        return data
    }
    if (typeof data === 'object') {
        Object.keys(data).forEach(key => {
            if (typeof data[key] === 'object') {
                data[key] = removeSpaces(data[key])
            } else if (typeof data[key] === 'string') {
                data[key] = data[key].replace(/(^\s*)|(\s*$)/g, '')
            }
        })
        return data
    } else if (typeof data === 'string') {
        return data.replace(/(^\s*)|(\s*$)/g, '')
    } else {
        return data
    }
}

/**
 * 判断两个数组是否相等
 * @param arr1 数组一
 * @package arr2 数组二
 */
export function arrIsSame (arr1, arr2) {
    function compare (prop) {
        return function (obj1, obj2) {
            let val1 = obj1[prop]
            let val2 = obj2[prop]
            if (val1 < val2) {
                return -1
            } else if (val1 > val2) {
                return 1
            } else {
                return 0
            }
        }
    }

    let array1 = arr1.sort(compare('permissionId'))
    let array2 = arr2.sort(compare('permissionId'))
    return JSON.stringify(array1) === JSON.stringify(array2)
}

/**
 * 数字转为金额格式 三位','
 * @param val 数字
 */
export function getMoneyNum (val) {
    if (val === 0 || val === '0') {
        return '0.00'
    }
    if (/\d/.test(val)) {
        if (!parseFloat(getCurLeng(val, 2)).toLocaleString().includes('.')) {
            return parseFloat(getCurLeng(val, 2)).toLocaleString() + '.00'
        } else {
            return parseFloat(getCurLeng(val, 2)).toLocaleString()
        }
    } else {
        return val
    }
}

/**
 *  保留几位小数，直接截取
 *  @param val 要转换的数据
 *  @param digits 保留几位
 */
export function getCurLeng (val, digits = 2) {
    val += ''
    if (val.includes('.')) {
        let num = val.substring(0, val.indexOf('.') + digits + 1)
        let s = String(num).length - (String(num).indexOf('.') + 1)
        if (s === 1) {
            return num + '0'
        } else {
            return num
        }
    } else {
        return val + '.00'
    }
}

/**
 *  保留几位小数，四舍五入
 *  @param val 要转换的数据
 *  @param digits 保留几位
 */
export function getZeroTwoNum (val, digits = 2) {
    val += ''
    if (val.includes('.')) {
        let n = Number(val).toFixed(digits) + ''
        if (n.split(':')[1] === '00') {
            return Number(n.substring(0, n.length - 3))
        } else {
            return Number(val).toFixed(digits)
        }
    } else {
        return Number(val)
    }
}

/**
 * 获取localStorage指定参数
 * @param params 参数名,嵌套关系,对数组无效
 */
export function getLocalStorage (...params) {
    if (!localStorage.getItem(params[0])) {
        return null
    }
    let ls = JSON.parse(localStorage.getItem(params[0]))
    if (params.length > 1) {
        let str = ls
        params.forEach((item, index) => {
            if (index !== 0) {
                str = str[item]
            }
        })
        return str
    } else {
        return ls
    }
}

/**
 * 获取排序字段
 * @param data 排序的行数据
 * @param map 表格的排序字段对应字符
 */
export function getSortString (data, map) {
    let type = data.order === 'ascending' ? ' asc' : ' desc'
    return data.order ? '`' + data.prop + '`' + type : ''
    // let prop = map[data.prop]
    // if (prop) {
    //     if ((data.prop).toLowerCase() === 'gbscore') {
    //         type = data.order === 'ascending' ? ' desc' : ' asc'
    //     }
    //     return data.order === null ? '' : prop + type
    // } else {
    //     return ''
    // }
}

/**
 * 处理导出数据返回
 * @param fileName 文件名
 * @param res 文件内容
 */
export function downloadData (fileName, res) {
    let name = parseTime(new Date().getTime()) + ' ' + fileName
    if (window.navigator.msSaveOrOpenBlob) {
        navigator.msSaveBlob(res, name)
    } else {
        let link = document.createElement('a')
        link.target = '_blank'
        link.href = window.URL.createObjectURL(res)
        link.download = name
        link.click()
        // 释放内存
        window.URL.revokeObjectURL(link.href)
    }
}

/**
 * 特殊处理权限需要隐藏内容（不使用auth指令）
 * @param val 权限code
 */
export function showBtnAuth (val) {
    let btnPowerArr = JSON.parse(localStorage.getItem('rolesCode'))
    if (btnPowerArr === undefined || btnPowerArr === null) {
        return false
    }
    if (btnPowerArr.includes(9999)) {
        return true
    }
    return btnPowerArr.indexOf(val) !== -1
}

/**
 * 计算参数配置的病案查看时间
 * @param val 出院时间
 */
export function viewInfoMonth (val) {
    // let month = localStorage.getItem('viewDeadlineDays')
    // if (!month) {
    //     return true
    // }
    // let future = new Date(dateNumFormat(val)).getTime() + month * 30 * 24 * 60 * 60 * 1000
    // let year = new Date().getFullYear()
    // let mon = new Date().getMonth() + 1
    // let day = new Date().getDate()
    // // 使用toLocaleDateString获取的时间格式根据浏览器不同不一致
    // let now = new Date(year + '/' + mon + '/' + day).getTime() + 8 * 60 * 60 * 1000
    // return future >= now
    return true
}

/**
 * 更新全局搜索对应页面store数据
 * @param name 转换数字
 * @param arr 参数对象集合
 */
export function setGetStore (name, ...arr) {
    let obj = JSON.parse(JSON.stringify(store.state.search.searchForm[name]))
    arr.forEach(item => {
        obj = Object.assign(obj, JSON.parse(JSON.stringify(item)))
    })
    store.dispatch('search/setSearchFormItem', {pageName: name, query: obj})
}

/**
 * 设置session储存tabs请求数据
 * @param name 路由名称
 * @param obj 参数对象
 */
export function setSession (name, obj) {
    if (sessionStorage.getItem(name)) {
        let old = JSON.parse(sessionStorage.getItem(name))
        sessionStorage.setItem(name, JSON.stringify(Object.assign({}, old, obj)))
    } else {
        sessionStorage.setItem(name, JSON.stringify(obj))
    }
}

/**
 * 获取session储存tabs请求数据
 * @param name 路由名称
 * @param methodName 方法名称
 */
export function getSession (name, methodName) {
    if (sessionStorage.getItem(name)) {
        let data = JSON.parse(sessionStorage.getItem(name))[methodName]
        if (data) {
            return data
        } else {
            return false
        }
    } else {
        return false
    }
}

/**
 * 报错提示添加span点击
 * @param str 目标字符串
 */
export function addSpan (str) {
    if (str === '') return str
    let reg = /[A-Za-z0-9.]+/g
    let wordList = []
    do {
        let arr = reg.exec(str)
        if (arr !== null) {
            wordList.push(arr[0])
        } else { break }
    } while (true)
    let res = str
    for (let i = 0; i < wordList.length; i++) {
        let splArr = res.split(wordList[i])
        res = splArr[0] + '<span class="tooltips-blue pointer">' + wordList[i] + '</span>' + splArr[1]
    }
    return res
}
