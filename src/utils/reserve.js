/**
 * 数字补零
 * @param str 传入数字字符串或数字
 * @return {string}
 */
export function padLeftZero (str) {
    if (typeof str !== 'string') {
        return ('00' + str.toString()).substr(str.toString().length)
    }
    return ('00' + str).substr(str.length)
}

/**
 * 合并两个对象，给出最后一个优先级
 * @param {Object} target
 * @param {(Object|Array)} source
 * @returns {Object}
 */
export function objectMerge (target, source) {
    if (typeof target !== 'object') {
        target = {}
    }
    if (Array.isArray(source)) {
        return source.slice()
    }
    Object.keys(source).forEach(property => {
        const sourceProperty = source[property]
        if (typeof sourceProperty === 'object') {
            target[property] = objectMerge(target[property], sourceProperty)
        } else {
            target[property] = sourceProperty
        }
    })
    return target
}
