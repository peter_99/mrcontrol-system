import Vue from 'vue'
import Router from 'vue-router'

/* Layout */
import Layout from '../views/layout/Layout'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
    independent: true            搜索条件独立，不继承到其他页面，不过可以继承一级页面条件
    returnPage: 'routerName'     返回按钮返回页面
  }
 **/
// 病案明细
const MedicalRecordsList = import('@/views/medical-records/medical-records-list')
// 病案详情
const MedicalRecordsInfo = import('@/views/medical-records/medical-records-info')
/**
 * meta说明
 * code 权限标识
 * pageQuery 地址栏参数需要保存，用于面包屑回跳
 * noLast 是否不显示末次统计 true是不显示
 * 注意：修改url和name时注意查找全局是否存在相应路由判断，同步修改
 */
export const constantRouterMap = [
    {path: '/login', component: () => import('@/views/login/index'), hidden: true},
    {path: '/selectSystem', component: () => import('@/views/select-system/index'), hidden: true},

    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        name: 'Dashboard',
        hidden: true,
        children: [{
            path: 'dashboard',
            component: () => import('@/views/dashboard/index')
        }]
    },
    {
        path: '/quality-overview',
        component: Layout,
        redirect: '/quality-overview/index',
        name: 'QualityOverview',
        meta: {title: '病案质量总览', icon: 'overview', code: 100001000000},
        children: [
            {
                path: 'index',
                name: 'OverviewIndex',
                component: () => import('@/views/quality-overview'),
                meta: {title: '病案质量总览', code: 100001000000, breadcrumb: false},
                hidden: true
            },
            {
                path: 'list',
                name: 'OverviewList',
                redirect: '/quality-overview/list/index',
                component: () => import('@/views/quality-overview/list'),
                meta: {title: '病案列表', code: 100001000000},
                hidden: true,
                children: [
                    {
                        path: 'index',
                        name: 'OverviewListIndex',
                        component: () => MedicalRecordsList,
                        meta: {title: '病案列表', code: 100001000000, pageQuery: true, independent: true, breadcrumb: false, returnPage: 'OverviewIndex'},
                        hidden: true
                    },
                    {
                        path: 'info',
                        name: 'OverviewListInfo',
                        component: () => MedicalRecordsInfo,
                        meta: {title: '病案详情', code: 100001000000, independent: true, returnPage: 'OverviewListIndex'},
                        hidden: true
                    }
                ]
            }
        ]
    },
    {
        path: '/importMR',
        component: Layout,
        redirect: '/importMR/index',
        name: 'ImportMR',
        meta: {title: '病案首页导入', icon: 'importmr', code: 100002000000, noLast: true},
        children: [
            {
                path: 'index',
                name: 'ImportMRIndex',
                component: () => import('@/views/importMR'),
                meta: {title: '病案首页导入', code: 100002000000, noLast: true, breadcrumb: false},
                hidden: true
            },
            {
                path: 'list',
                name: 'ImportMRList',
                redirect: '/importMR/list/index',
                component: () => import('@/views/importMR/list'),
                meta: {title: '病案列表', code: 100002000000, pageQuery: true, noLast: true},
                hidden: true,
                children: [
                    {
                        path: 'index',
                        name: 'ImportMRListIndex',
                        component: () => MedicalRecordsList,
                        meta: {title: '病案列表', code: 100002000000, pageQuery: true, breadcrumb: false, independent: true, returnPage: 'ImportMRIndex'},
                        hidden: true
                    },
                    {
                        path: 'info',
                        name: 'ImportMRListInfo',
                        component: () => MedicalRecordsInfo,
                        meta: {title: '病案详情', code: 100002000000, independent: true, returnPage: 'ImportMRListIndex'},
                        hidden: true
                    }
                ]
            }
        ]
    },
    {
        path: '/MRList',
        component: Layout,
        redirect: '/MRList/index',
        name: 'MRList',
        meta: {title: '病案首页列表', icon: 'mrlist', code: 100003000000, noLast: true},
        children: [
            {
                path: 'index',
                name: 'MRListIndex',
                component: () => MedicalRecordsList,
                meta: {title: '病案首页列表', code: 100003000000, pageQuery: true, breadcrumb: false, independent: true},
                hidden: true
            },
            {
                path: 'info',
                name: 'MRListInfo',
                component: () => MedicalRecordsInfo,
                meta: {title: '病案详情', code: 100003000000, independent: true, returnPage: 'MRListIndex'},
                hidden: true
            }
        ]
    },
    {
        path: '/quality-control',
        component: Layout,
        redirect: '/quality-control/score',
        name: 'QualityControl',
        meta: {title: '病案质控统计', icon: 'control', code: 100004000000},
        children: [
            {
                path: 'score',
                name: 'Score',
                redirect: '/quality-control/score/index',
                component: () => import('@/views/quality-control'),
                meta: {title: '评分统计', code: 100004001000, barIndex: 'score'},
                children: [
                    {
                        path: 'index',
                        name: 'ScoreIndex',
                        component: () => import('@/views/quality-control/score/statistics'),
                        meta: {title: '评分统计', code: 100004001000, barIndex: 'score', breadcrumb: false},
                        hidden: true
                    },
                    {
                        path: 'list',
                        name: 'ScoreList',
                        redirect: '/quality-control/score/list/index',
                        component: () => import('@/views/quality-control'),
                        meta: {title: '病案列表', code: 100004001000, pageQuery: true, barIndex: 'score'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'ScoreListIndex',
                                component: () => MedicalRecordsList,
                                meta: {title: '病案列表', code: 100004001000, pageQuery: true, barIndex: 'score', breadcrumb: false, independent: true, returnPage: 'ScoreIndex'},
                                hidden: true
                            },
                            {
                                path: 'info',
                                name: 'ScoreListInfo',
                                component: () => MedicalRecordsInfo,
                                meta: {title: '病案详情', code: 100004001000, barIndex: 'score', independent: true, returnPage: 'ScoreListIndex'},
                                hidden: true
                            }
                        ]
                    }
                ]
            },
            {
                path: 'coding-defect',
                name: 'CodingDefect',
                redirect: '/quality-control/coding-defect/index',
                component: () => import('@/views/quality-control'),
                meta: {title: '编码缺陷统计', code: 100004002000, barIndex: 'coding'},
                children: [
                    {
                        path: 'index',
                        name: 'CodingIndex',
                        component: () => import('@/views/quality-control/coding-defect/statistics'),
                        meta: {title: '编码缺陷统计', code: 100004002000, barIndex: 'coding', breadcrumb: false},
                        hidden: true
                    },
                    {
                        path: 'list',
                        name: 'CodingList',
                        redirect: '/quality-control/coding-defect/list/index',
                        component: () => import('@/views/quality-control'),
                        meta: {title: '病案列表', code: 100004002000, pageQuery: true, barIndex: 'coding'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'CodingListIndex',
                                component: () => MedicalRecordsList,
                                meta: {title: '病案列表', code: 100004002000, pageQuery: true, barIndex: 'coding', breadcrumb: false, independent: true, returnPage: 'CodingIndex'},
                                hidden: true
                            },
                            {
                                path: 'info',
                                name: 'CodingListInfo',
                                component: () => MedicalRecordsInfo,
                                meta: {title: '病案详情', code: 100004002000, barIndex: 'coding', independent: true, returnPage: 'CodingListIndex'},
                                hidden: true
                            }
                        ]
                    }
                ]
            },
            {
                path: 'non-coding-defect',
                name: 'NonCodingDefect',
                redirect: '/quality-control/non-coding-defect/index',
                component: () => import('@/views/quality-control'),
                meta: {title: '非编码缺陷统计', code: 100004003000, barIndex: 'non-coding'},
                children: [
                    {
                        path: 'index',
                        name: 'NonCodingIndex',
                        component: () => import('@/views/quality-control/non-coding-defect/statistics'),
                        meta: {title: '非编码缺陷统计', code: 100004003000, barIndex: 'non-coding', breadcrumb: false},
                        hidden: true
                    },
                    {
                        path: 'list',
                        name: 'NonCodingList',
                        redirect: '/quality-control/non-coding-defect/list/index',
                        component: () => import('@/views/quality-control'),
                        meta: {title: '病案列表', code: 100004003000, pageQuery: true, barIndex: 'non-coding'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'NonCodingListIndex',
                                component: () => MedicalRecordsList,
                                meta: {title: '病案列表', code: 100004003000, pageQuery: true, barIndex: 'non-coding', breadcrumb: false, independent: true, returnPage: 'NonCodingIndex'},
                                hidden: true
                            },
                            {
                                path: 'info',
                                name: 'NonCodingListInfo',
                                component: () => MedicalRecordsInfo,
                                meta: {title: '病案详情', code: 100004003000, barIndex: 'non-coding', independent: true, returnPage: 'NonCodingListIndex'},
                                hidden: true
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        path: '/summary-list',
        component: Layout,
        redirect: '/summary-list/department',
        name: 'SummaryList',
        meta: {title: '汇总列表', icon: 'summarylist', code: 100005000000, noLast: true},
        children: [
            {
                path: 'department',
                name: 'DepSummary',
                component: () => import('@/views/summary-list'),
                redirect: '/summary-list/department/list',
                meta: {title: '科室汇总列表', code: 100005001000, barIndex: 'department', noLast: true},
                children: [
                    {
                        path: 'list',
                        name: 'DepSummaryList',
                        component: () => import('@/views/summary-list/department/list'),
                        meta: {title: '科室汇总列表', code: 100005001000, barIndex: 'department', breadcrumb: false},
                        hidden: true
                    },
                    {
                        path: 'mrlist',
                        name: 'DepSummaryMR',
                        redirect: '/summary-list/department/mrlist/index',
                        component: () => import('@/views/summary-list'),
                        meta: {title: '病案列表', code: 100005001000, pageQuery: true, barIndex: 'department'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'DepSummaryMRList',
                                component: () => MedicalRecordsList,
                                meta: {title: '病案列表', code: 100005001000, pageQuery: true, barIndex: 'department', breadcrumb: false, independent: true, returnPage: 'DepSummaryList'},
                                hidden: true
                            },
                            {
                                path: 'info',
                                name: 'DepSummaryMRInfo',
                                component: () => MedicalRecordsInfo,
                                meta: {title: '病案详情', code: 100005001000, barIndex: 'department', independent: true, returnPage: 'DepSummaryMRList'},
                                hidden: true
                            }
                        ]
                    },
                    {
                        path: 'analysis',
                        name: 'DepSummaryAnalysis',
                        redirect: '/summary-list/department/analysis/index',
                        component: () => import('@/views/summary-list'),
                        meta: {title: '科室缺陷类型分析', code: 100005001000, pageQuery: true, barIndex: 'department'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'DepSummaryAnalysisIndex',
                                component: () => import('@/views/summary-list/department/analysis'),
                                meta: {title: '科室缺陷类型分析', code: 100005001000, pageQuery: true, breadcrumb: false, barIndex: 'department', independent: true, returnPage: 'DepSummaryList'},
                                hidden: true
                            },
                            {
                                path: 'mrlist',
                                name: 'DepSummaryAnalysisMR',
                                redirect: '/summary-list/department/analysis/mrlist/index',
                                component: () => import('@/views/summary-list'),
                                meta: {title: '病案列表', code: 100005001000, pageQuery: true, barIndex: 'department'},
                                hidden: true,
                                children: [
                                    {
                                        path: 'index',
                                        name: 'DepSummaryAnalysisMRList',
                                        component: () => MedicalRecordsList,
                                        meta: {title: '病案列表', code: 100005001000, pageQuery: true, barIndex: 'department', breadcrumb: false, independent: true, returnPage: 'DepSummaryAnalysisIndex'},
                                        hidden: true
                                    },
                                    {
                                        path: 'info',
                                        name: 'DepSummaryAnalysisMRInfo',
                                        component: () => MedicalRecordsInfo,
                                        meta: {title: '病案详情', code: 100005001000, barIndex: 'department', independent: true, returnPage: 'DepSummaryAnalysisMRList'},
                                        hidden: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                path: 'doctor',
                name: 'DoctorSummary',
                component: () => import('@/views/summary-list'),
                redirect: '/summary-list/doctor/list',
                meta: {title: '医师汇总列表', code: 100005002000, barIndex: 'doctor'},
                children: [
                    {
                        path: 'list',
                        name: 'DoctorSummaryList',
                        component: () => import('@/views/summary-list/doctor/list'),
                        meta: {title: '医师汇总列表', code: 100005002000, barIndex: 'doctor', breadcrumb: false},
                        hidden: true
                    },
                    {
                        path: 'mrlist',
                        name: 'DoctorSummaryMR',
                        redirect: '/summary-list/doctor/mrlist/index',
                        component: () => import('@/views/summary-list'),
                        meta: {title: '病案列表', code: 100005002000, pageQuery: true, barIndex: 'doctor'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'DoctorSummaryMRList',
                                component: () => MedicalRecordsList,
                                meta: {title: '病案列表', code: 100005002000, pageQuery: true, barIndex: 'doctor', breadcrumb: false, independent: true, returnPage: 'DoctorSummaryList'},
                                hidden: true
                            },
                            {
                                path: 'info',
                                name: 'DoctorSummaryInfo',
                                component: () => MedicalRecordsInfo,
                                meta: {title: '病案详情', code: 100005002000, barIndex: 'doctor', independent: true, returnPage: 'DoctorSummaryMRList'},
                                hidden: true
                            }
                        ]
                    },
                    {
                        path: 'analysis',
                        name: 'DoctorSummaryAnalysis',
                        redirect: '/summary-list/doctor/analysis/index',
                        component: () => import('@/views/summary-list'),
                        meta: {title: '医师缺陷类型分析', code: 100005002000, pageQuery: true, barIndex: 'doctor'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'DoctorSummaryAnalysisIndex',
                                component: () => import('@/views/summary-list/doctor/analysis'),
                                meta: {title: '医师缺陷类型分析', code: 100005002000, pageQuery: true, breadcrumb: false, barIndex: 'doctor', independent: true, returnPage: 'DoctorSummaryList'},
                                hidden: true
                            },
                            {
                                path: 'mrlist',
                                name: 'DoctorSummaryAnalysisMR',
                                redirect: '/summary-list/coder/analysis/mrlist/index',
                                component: () => import('@/views/summary-list'),
                                meta: {title: '病案列表', code: 100005002000, pageQuery: true, barIndex: 'doctor'},
                                hidden: true,
                                children: [
                                    {
                                        path: 'index',
                                        name: 'DoctorSummaryAnalysisMRList',
                                        component: () => MedicalRecordsList,
                                        meta: {title: '病案列表', code: 100005002000, pageQuery: true, barIndex: 'doctor', breadcrumb: false, independent: true, returnPage: 'DoctorSummaryAnalysisIndex'},
                                        hidden: true
                                    },
                                    {
                                        path: 'info',
                                        name: 'DoctorSummaryAnalysisMRInfo',
                                        component: () => MedicalRecordsInfo,
                                        meta: {title: '病案详情', code: 100005002000, barIndex: 'doctor', independent: true, returnPage: 'DoctorSummaryAnalysisMRList'},
                                        hidden: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                path: 'coder',
                name: 'CoderSummary',
                component: () => import('@/views/summary-list'),
                redirect: '/summary-list/coder/list',
                meta: {title: '编码员汇总列表', code: 100005003000, barIndex: 'coder'},
                children: [
                    {
                        path: 'list',
                        name: 'CoderSummaryList',
                        component: () => import('@/views/summary-list/coder/list'),
                        meta: {title: '编码员汇总列表', code: 100005003000, barIndex: 'coder', breadcrumb: false},
                        hidden: true
                    },
                    {
                        path: 'mrlist',
                        name: 'CoderSummaryMR',
                        redirect: '/summary-list/coder/mrlist/index',
                        component: () => import('@/views/summary-list'),
                        meta: {title: '病案列表', code: 100005003000, pageQuery: true, barIndex: 'coder'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'CoderSummaryMRList',
                                component: () => MedicalRecordsList,
                                meta: {title: '病案列表', code: 100005003000, pageQuery: true, barIndex: 'coder', breadcrumb: false, independent: true, returnPage: 'CoderSummaryList'},
                                hidden: true
                            },
                            {
                                path: 'info',
                                name: 'CoderSummaryMRInfo',
                                component: () => MedicalRecordsInfo,
                                meta: {title: '病案详情', code: 100005003000, barIndex: 'coder', independent: true, returnPage: 'CoderSummaryMRList'},
                                hidden: true
                            }
                        ]
                    },
                    {
                        path: 'analysis',
                        name: 'CoderSummaryAnalysis',
                        redirect: '/summary-list/coder/analysis/index',
                        component: () => import('@/views/summary-list'),
                        meta: {title: '编码员缺陷类型分析', code: 100005003000, pageQuery: true, barIndex: 'coder'},
                        hidden: true,
                        children: [
                            {
                                path: 'index',
                                name: 'CoderSummaryAnalysisIndex',
                                component: () => import('@/views/summary-list/coder/analysis'),
                                meta: {title: '编码员缺陷类型分析', code: 100005003000, pageQuery: true, breadcrumb: false, barIndex: 'coder', independent: true, returnPage: 'CoderSummaryList'},
                                hidden: true
                            },
                            {
                                path: 'mrlist',
                                name: 'CoderSummaryAnalysisMR',
                                redirect: '/summary-list/coder/analysis/mrlist/index',
                                component: () => import('@/views/summary-list'),
                                meta: {title: '病案列表', code: 100005003000, pageQuery: true, barIndex: 'coder'},
                                hidden: true,
                                children: [
                                    {
                                        path: 'index',
                                        name: 'CoderSummaryAnalysisMRList',
                                        component: () => MedicalRecordsList,
                                        meta: {title: '病案列表', code: 100005003000, pageQuery: true, barIndex: 'coder', breadcrumb: false, independent: true, returnPage: 'CoderSummaryAnalysisIndex'},
                                        hidden: true
                                    },
                                    {
                                        path: 'info',
                                        name: 'CoderSummaryAnalysisMRInfo',
                                        component: () => MedicalRecordsInfo,
                                        meta: {title: '病案详情', code: 100005003000, barIndex: 'coder', independent: true, returnPage: 'CoderSummaryAnalysisMRList'},
                                        hidden: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        path: '/qc-effect',
        component: Layout,
        redirect: '/qc-effect/index',
        name: 'QCEffect',
        meta: {title: '质控效果分析', icon: 'qceffect', code: 100006000000, noLast: true},
        children: [
            {
                path: 'index',
                name: 'QCEffectIndex',
                component: () => import('@/views/qc-effect'),
                meta: {title: '质控效果分析', code: 100006000000, noLast: true, barIndex: 'effect', breadcrumb: false},
                hidden: true
            },
            {
                path: 'info',
                name: 'QCEffectInfo',
                component: () => MedicalRecordsInfo,
                meta: {title: '病案详情', code: 100006000000, pageQuery: true, barIndex: 'effect', independent: true, returnPage: 'QCEffectIndex'},
                hidden: true
            },
            {
                path: 'mrlist',
                name: 'QCEffectMR',
                redirect: '/qc-effect/mrlist/index',
                component: () => import('@/views/qc-effect/list'),
                meta: {title: '病案列表', code: 100006000000, pageQuery: true, barIndex: 'coder'},
                hidden: true,
                children: [
                    {
                        path: 'index',
                        name: 'QCEffectMRList',
                        component: () => MedicalRecordsList,
                        meta: {title: '病案列表', code: 100006000000, pageQuery: true, barIndex: 'coder', breadcrumb: false, independent: true, returnPage: 'QCEffectIndex'},
                        hidden: true
                    },
                    {
                        path: 'info',
                        name: 'QCEffectMRInfo',
                        component: () => MedicalRecordsInfo,
                        meta: {title: '病案详情', code: 100006000000, barIndex: 'coder', independent: true, returnPage: 'QCEffectMRInfo'},
                        hidden: true
                    }
                ]
            },
            {
                path: 'code',
                name: 'QCEffectCode',
                component: () => import('@/views/qc-effect/code/list'),
                meta: {title: '修改项详情(编码)', code: 100006000000, noLast: true, barIndex: 'effect', independent: true, returnPage: 'QCEffectIndex'},
                hidden: true
            },
            {
                path: 'noncode',
                name: 'QCEffectNonCode',
                component: () => import('@/views/qc-effect/noncode/list'),
                meta: {title: '修改项详情(非编码)', code: 100006000000, noLast: true, barIndex: 'effect', independent: true, returnPage: 'QCEffectIndex'},
                hidden: true
            }
        ]
    },
    {
        path: '/myMR',
        component: Layout,
        redirect: '/myMR/audit',
        name: 'MyMR',
        meta: {title: '我的病案', icon: 'myMR', code: 999999999999, noLast: true},
        children: [
            {
                path: 'audit',
                name: 'MyMRAudit',
                redirect: '/myMR/audit/index',
                component: () => import('@/views/myMR'),
                meta: {title: '我的审核', code: 999999999999, noLast: true},
                children: [
                    {
                        path: 'index',
                        name: 'MyMRAuditIndex',
                        component: () => import('@/views/myMR/audit'),
                        meta: {title: '我的审核', code: 999999999999, pageQuery: true, breadcrumb: false, independent: true, returnPage: 'MyMRAudit'},
                        hidden: true
                    },
                    {
                        path: 'info',
                        name: 'MyMRAuditInfo',
                        component: () => MedicalRecordsInfo,
                        meta: {title: '病案详情', code: 999999999999, independent: true, returnPage: 'MyMRAudit'},
                        hidden: true
                    }
                ]
            },
            {
                path: 'collect',
                name: 'MyMRCollect',
                redirect: '/myMR/collect/index',
                component: () => import('@/views/myMR'),
                meta: {title: '我的收藏', code: 999999999999, pageQuery: true, noLast: true},
                children: [
                    {
                        path: 'index',
                        name: 'MyMRCollectIndex',
                        component: () => import('@/views/myMR/collect'),
                        meta: {title: '我的收藏', code: 999999999999, pageQuery: true, breadcrumb: false, independent: true, returnPage: 'MyMRCollect'},
                        hidden: true
                    },
                    {
                        path: 'info',
                        name: 'MyMRCollectInfo',
                        component: () => MedicalRecordsInfo,
                        meta: {title: '病案详情', code: 999999999999, independent: true, returnPage: 'MyMRCollect'},
                        hidden: true
                    }
                ]
            }
        ]
    },
    {
        path: '/introduction',
        component: Layout,
        redirect: '/introduction/index',
        name: 'Introduction',
        meta: {title: '产品介绍'},
        hidden: true,
        children: [
            {
                path: 'index',
                name: 'Intor',
                component: () => import('@/views/introduction'),
                meta: {title: '产品介绍', breadcrumb: false},
                hidden: true
            }
        ]
    }
]

export default new Router({
    mode: 'history', // 后端支持可开
    base: 'mrqc', // 这里设置的是部署在子站点的名字
    scrollBehavior: () => ({y: 0}),
    routes: constantRouterMap
})
